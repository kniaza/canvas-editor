import Konva from 'konva';
import Toolbar from './controllers/toolbar';
import Draw from './controllers/draw';
import Transformer from './controllers/transformer';
import History from './controllers/history';
import ExportImport from './controllers/export-import';

const toolbar = new Toolbar('#toolbar .tools');

const stage = new Konva.Stage({
  container: 'container',
  width: 800,
  height: 600,
});

function init(container = stage) {
  const layer = new Konva.Layer();
  const tr = new Konva.Transformer();
  const transformerController = new Transformer(tr);
  const history = new History('#toolbar .history', container, layer, init);
  const drawActions = new Draw(toolbar, layer, history);
  const exportImport = new ExportImport(
    '#toolbar .import-export',
    container,
    layer,
    history
  );

  container.on('mousedown', (e) => {
    const obj = e.target;
    if (obj === container) {
      return;
    }

    if (!(obj.parent instanceof Konva.Transformer)) {
      transformerController.selectObject(obj);
    }
  });

  container.on('click', (e) => {
    const mousePos = container.getPointerPosition();
    drawActions.setMousePosition(mousePos);

    if (toolbar.getMode() === 'select') {
      if (e.target === container) {
        // if click on empty area - remove all selections
        transformerController.clearSelectedObject();
      } else {
        // Select object on stage
        transformerController.selectObject(e.target);
      }
    }

    drawActions.draw();
  });

  // add the layer to the stage
  container.add(layer);

  // add the Tranfrom object to the layer
  layer.add(tr);

  // draw the image
  layer.draw();
}

init();
