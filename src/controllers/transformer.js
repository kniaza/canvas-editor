function Transformer(transfromObject) {
  this.clearSelectedObject = () => {
    transfromObject.nodes([]);
  };

  this.selectObject = (object) => {
    transfromObject.nodes([object]);
  };
}

export default Transformer;
