import Circle from './circle';
import Rect from './rect';

/**
 * draw object on canvas
 * @param  {strin} mode - rect | circle | etc.
 * @param  {object} layer
 * @param  {object} history
 * @return {void}
 */
function Draw(toolbar, layer, history) {
  let object = null;
  this.mousePostion = { x: 0, y: 0 };

  this.setMousePosition = (postion) => {
    this.mousePostion = postion;
  };

  const makeNewHistory = () => {
    history.updateHistory();
    console.log('make new history', history);
  };

  this.draw = () => {
    const mode = toolbar.getMode();
    if (null || mode === 'select') return;

    switch (mode) {
      case 'circle':
        object = new Circle(this.mousePostion, makeNewHistory);
        break;
      case 'rect':
        object = new Rect(this.mousePostion, makeNewHistory);
        break;
    }

    layer.add(object);
    layer.draw();
    makeNewHistory();
  };
}

export default Draw;
