import Konva from 'konva';

function ExportImport(elementQuery, stage, layer, history) {
  const element = document.querySelector(elementQuery);

  const downloadURI = (uri, name) => {
    var link = document.createElement('a');
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    // delete link;
  };

  const changeFile = (e) => {
    const URL = window.webkitURL || window.URL;
    const url = URL.createObjectURL(e.target.files[0]);
    const img = new Image();
    img.src = url;

    img.onload = (e) => {
      const img_width = img.width;
      const img_height = img.height;

      // calculate dimensions to get max 300px
      const max = 300;
      const ratio = img_width > img_height ? img_width / max : img_height / max;

      const theImg = new Konva.Image({
        image: img,
        x: 50,
        y: 30,
        width: img_width / ratio,
        height: img_height / ratio,
        draggable: true,
      });

      layer.add(theImg);
      layer.draw();
      history.updateHistory();
    };

    e.target.value = '';
  };

  element
    .querySelector('input[type="file"]')
    .addEventListener('change', changeFile);

  element.querySelectorAll('button').forEach((item) => {
    item.addEventListener('click', (e) => {
      const action = e.target.dataset.name;
      if (action === 'export') {
        console.log('export');
        const dataURL = stage.toDataURL();
        downloadURI(dataURL, 'stage.png');
      }
    });
  });
}

export default ExportImport;
