import Konva from 'konva';

function History(elementQuery, stage, layer, init, callback) {
  const element = document.querySelector(elementQuery);
  this.history = [stage.toJSON()];
  this.step = 0;

  this.updateHistory = () => {
    this.history.push(stage.toJSON());
    this.step += 1;
  };

  this.undoHistory = () => {
    if (this.step === 0) return;
    layer.destroy();
    const newStage = Konva.Node.create(
      this.history[this.step - 1],
      'container'
    );
    this.step -= 1;
    this.history.splice(-1);
    init(newStage);
  };

  element.querySelectorAll('button').forEach((item) => {
    item.addEventListener('click', (e) => {
      const action = e.target.dataset.name;
      if (action === 'undo') {
        this.undoHistory();
      }
    });
  });
}

export default History;
