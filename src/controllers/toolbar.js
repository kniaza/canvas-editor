function Toolbar(elementQuery) {
  const element = document.querySelector(elementQuery);
  let selectedMode = 'select';
  this.modeList = ['select', 'circle', 'rect', 'connect'];

  this.setMode = (modeName) => {
    selectedMode = modeName;
  };

  this.getMode = () => selectedMode;

  const buttons = element.querySelectorAll('button');

  buttons.forEach((item) => {
    item.addEventListener('click', (e) => {
      this.setMode(e.target.dataset.name);
      buttons.forEach((button) => button.classList.remove('active'));
      e.target.classList.add('active');
      console.log(selectedMode);
    });
  });
}

export default Toolbar;
