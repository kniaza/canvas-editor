function Circle(position, callback) {
  const circle = new Konva.Circle({
    x: position.x,
    y: position.y,
    radius: 15,
    fill: 'red',
    stroke: 'black',
    strokeWidth: 2,
    draggable: true,
  });

  this.getCircle = () => circle;

  this.initEvents = () => {
    circle.on('dragend', (e) => {
      if (callback instanceof Function) callback();
      console.log('dragend');
    });

    circle.on('transformend', (e) => {
      if (callback instanceof Function) callback();
      console.log('transformend');
    });
  };

  this.initEvents();

  return circle;
}

export default Circle;
