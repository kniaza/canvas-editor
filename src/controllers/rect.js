function Rect(position, callback) {
  const rect = new Konva.Rect({
    x: position.x,
    y: position.y,
    width: 20,
    height: 20,
    fill: 'red',
    stroke: 'black',
    strokeWidth: 2,
    draggable: true,
  });

  this.getCircle = () => rect;

  this.initEvent = () => {
    rect.on('dragend transformend', (e) => {
      if (callback instanceof Function) callback();
    });
  };

  this.initEvent();
  return rect;
}

export default Rect;
